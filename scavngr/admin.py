from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Player)
admin.site.register(Hunt)
admin.site.register(Clue)
admin.site.register(ActiveGame)
admin.site.register(HuntClue)
