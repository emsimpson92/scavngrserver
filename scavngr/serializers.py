from .models import *
from rest_framework import serializers

class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ['playerID', 'username', 'password', 'huntsAttempted', 'huntsCompleted']

class HuntSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hunt
        fields = ['huntID', 'name', 'description', 'latitude', 'longitude', 'maxPlayers', 'currentPlayers']

class ClueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clue
        fields = ['clueID', 'clueText', 'latitude', 'longitude', 'image']

class HuntClueSerializer(serializers.ModelSerializer):
    class Meta:
        model = HuntClue
        fields = ['huntID', 'clueID', 'clueNumber']

class ActiveGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActiveGame
        fields = ['playerID', 'huntID', 'currentClue']
