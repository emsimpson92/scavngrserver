from django.shortcuts import render
from .models import *
from django.http import HttpRequest, HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
import re
import json

# Create your views here.
def signup(request):
    # sign up
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = PlayerSerializer(data = data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 200)
        else:
            print(serializer.errors)
            return HttpResponse("Error encountered during signup!")

def login(request):
    # Login and return active hunt if not null
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            player = Player.players.get(username = data["username"])
        except Player.DoesNotExist:
            return HttpResponse("Invalid Username!")
        #TODO: password encryption
        if player.password == data["password"]:
            try:
                currentHunt = ActiveGame.activeGames.get(playerID = player.playerID)
                hunt = Hunt.hunts.get(huntID = currentHunt.huntID)
                serializer = HuntSerializer(hunt)
                return JsonResponse(serializer.data, status = 200)
            except ActiveGame.DoesNotExist:
                return HttpResponse("Logged in successfully!")
        else:
            return HttpResponse("Invalid Password!")

def hunts(request):
    # Get nearby hunts that aren't full
    if request.method == 'GET':
        data = JSONParser().parse(request)
        allHunts = Hunt.hunts.all()
        huntList = []
        MAP_THRESHOLD = 1.5 # Approximately 20 miles
        for hunt in allHunts:
            distance = abs((float(data["lat"]) - float(hunt.latitude)) ** 2 + (float(data["lon"]) - float(hunt.longitude)) ** 2)
            if distance <= MAP_THRESHOLD and (hunt.currentPlayers < hunt.maxPlayers or hunt.maxPlayers == None):
                huntList.append(hunt)

        serializer = HuntSerializer(huntList, many = True)
        return JsonResponse(serializer.data, safe = False, satus = 200)

def activeGames(request):
    data = JSONParser().parse(request)
    # Start a new hunt if none are currently started and hunt isn't full
    if request.method == 'POST':
        try:
            currentHunt = ActiveGame.activeGames.get(playerID = data["playerID"])
            return HttpResponse("Abandon your current hunt before starting a new one!")
        except ActiveGame.DoesNotExist:
            hunt = Hunt.hunts.get(huntID = data["huntID"])
            if hunt.currentPlayers < hunt.maxPlayers:
                hunt.currentPlayers += 1
                hunt.save()
            else:
                return HttpResponse("Hunt is full!")
            player = Player.players.get(playerID = data["playerID"])
            player.huntsAttempted += 1
            player.save()
            
            serializer = ActiveGameSerializer(data = data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, status = 200)
            else:
                print(serializer.errors)
                return HttpResponse("Error selecting hunt!")

    # When the user finds a clue, returns the next clue
    elif request.method == 'PUT':
        try:
            currentHunt = ActiveGame.activeGames.get(playerID = data["playerID"])
            currentHunt.currentClue = data["currentClue"]
            currentHunt.save()
            huntClue = HuntClue.huntClues.get(clueNumber = data["currentClue"])
            clue = Clue.clues.get(clueID = huntClue.clueID)
            serializer = ClueSerializer(clue)
            return JsonResponse(serializer.data, status = 200)
        except ActiveGame.DoesNotExist:
            return HttpResponse("You are not currently on a hunt!")

    # Retrieve current and all past clues for the player's hunt
    elif request.method == 'GET':
        try:
            currentHunt = ActiveGame.activeGames.get(playerID = data["playerID"])
            currentClue = currentHunt.currentClue
            huntClues = HuntClue.huntClues.filter(clueNumber <= currentClue)
            clues = []
            for hc in huntClues:
                clue = Clue.clues.get(clueID = hc.clueID)
                clues.append(clue)
            serializer = ClueSerializer(clues, many = True)
            return JsonResponse(serializer.data, safe = False, status = 200)

        except ActiveGame.DoesNotExist:
            return HttpResponse("You are not currently on a hunt!")

    # Abandon the hunt (finishing a hunt will be handled differently)
    elif request.method == 'DELETE':
        hunt = Hunt.hunts.get(huntID = data["huntID"])
        hunt.currentPlayers -= 1
        hunt.save()
        currentHunt = ActiveGame.activeGames.get(playerID = data["playerID"])
        currentHunt.delete()

        return HttpResponse("Hunt abandoned!")
