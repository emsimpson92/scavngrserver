from django.db import models

# Create your models here.
class Hunt(models.Model):
    huntID = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 30)
    description = models.CharField(max_length = 255)
    latitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    longitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    maxPlayers = models.IntegerField()
    currentPlayers = models.IntegerField()

    hunts = models.Manager()

    class Meta:
        managed = True
        db_table = "Hunts"

class Player(models.Model):
    playerID = models.AutoField(primary_key = True)
    username = models.CharField(max_length = 30)
    password = models.CharField(max_length = 30)
    huntsAttempted = models.IntegerField()
    huntsCompleted = models.IntegerField()

    players = models.Manager()

    class Meta:
        managed = True
        db_table = "Players"

class Clue(models.Model):
    clueID = models.AutoField(primary_key = True)
    clueText = models.CharField(max_length = 255)
    latitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    longitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    image = models.CharField(max_length = 255)

    clues = models.Manager()

    class Meta:
        managed = True
        db_table = "Clues"

class HuntClue(models.Model):
    huntID = models.IntegerField()
    clueID = models.IntegerField()
    clueNumber = models.IntegerField()

    huntClues = models.Manager()

    class Meta:
        managed = True
        db_table = "HuntClues"

class ActiveGame(models.Model):
    playerID = models.IntegerField()
    huntID = models.IntegerField()
    currentClue = models.IntegerField()

    activeGames = models.Manager()

    class Meta:
        managed = True
        db_table = "ActiveGames"
